import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entitty/product.entitty';

let products: Product[] = [
  { id: 1, name: 'PS5', price: 18000 },
  { id: 2, name: 'Xbox series x', price: 16000 },
  { id: 3, name: 'Nintendo switch', price: 15000 },
];
let lastProductId = 4;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
      price: 0
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = products[index];
    products.splice(index, 1);
    return deleteProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'PS5', price: 18000 },
      { id: 2, name: 'Xbox series x', price: 16000 },
      { id: 3, name: 'Nintendo switch', price: 15000 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}